from io_terminal import imprime_lista

nome_ficheiro_lista_de_veiculos = "lista_de_veiculos.pk"
"""
o nome da lista é lista_de_veiculos.pk
"""

def cria_novo_veiculo():
    """ Pede ao utilizador para introduzir um novo veiculo

    :return: dicionario com um veiculo na forma
        {"marca": <<marca>>, "modelo": <<modelo>>, "cor": <<cor>>, "comprimento": <<comprimento>>, "cm3": <<cm3>>, "peso": <<peso>>, "matricula": <<matricula>>}
    """

    marca = input("marca? ")
    modelo = input("modelo? ")
    cor = input("cor? ")
    comprimento = int(input("comprimento? "))
    cm3 = int(input("cm3? "))
    peso = int(input("peso? "))
    matricula = input("matricula? ").upper()
    return {"marca": marca, "modelo": modelo, "cor": cor, "comprimento": comprimento, "cm3": cm3, "peso": peso,
            "matricula": matricula}

"""
dados para o trabalhador do stand introduzi sobre os veiculos
"""

def imprime_lista_de_veiculos(lista_de_veiculos):
    """ ..."""

    imprime_lista(cabecalho="Lista de Veiculos", lista=lista_de_veiculos)
