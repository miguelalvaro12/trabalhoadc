from veiculos import (cria_novo_veiculo,
                          imprime_lista_de_veiculos,
                          nome_ficheiro_lista_de_veiculos
                          )
from utilizadores import (cria_novo_utilizador,
                              imprime_lista_de_utilizadores,
                              nome_ficheiro_lista_de_utilizadores
                              )
from io_ficheiros import (carrega_as_listas_dos_ficheiros,
                          guarda_as_listas_em_ficheiros
                          )
from io_terminal import pergunta_id
import time

def menu():
    """ main menu da aplicação"""

    lista_de_veiculos = []
    lista_de_utilizadores = []
    lista_de_compras = []

    while True:
        print("""
        *********************************************************************
        :               STAND EBEER - OS CLASSICOS DAS 2 RODAS              : 
        *********************************************************************
        :                                                                   :
        : vn - adicionar novo veiculo         vl - lista veiculos           :
        : av - apagar o ultimo veiculo        un - adicionar novo utilizador:
        : au - apagar o ultimo utilizador     ul - lista utilizadores       :
        : cn - adicionar nova compra          cl - lista compras            :
        : ...                                                               :
        : g - guardar tudo           c - carregar tudo                      :
        : x - sair                                                          :
        :                                                                   :
        *********************************************************************
        """)

        op = input("opcao?").lower()

        if op == "x":
            exit()
        elif op == "vn":
            novo_veiculo = cria_novo_veiculo()
            lista_de_veiculos.append(novo_veiculo)
        elif op == "av":
            lista_de_veiculos.remove(novo_veiculo)
        elif op == "vl":
            imprime_lista_de_veiculos(lista_de_veiculos)
        elif op == "un":
            novo_utilizador = cria_novo_utilizador()
            lista_de_utilizadores.append(novo_utilizador)
        elif op == "au":
            lista_de_utilizadores.remove(novo_utilizador)
        elif op == "ul":
            imprime_lista_de_utilizadores(lista_de_utilizadores)
        elif op == "g":
            guarda_as_listas_em_ficheiros(lista_de_veiculos,
                                          lista_de_utilizadores,
                                          nome_ficheiro_lista_de_veiculos,
                                          nome_ficheiro_lista_de_utilizadores
                                          )
        elif op == "c":
            lista_de_veiculos, lista_de_utilizadores = carrega_as_listas_dos_ficheiros(
                nome_ficheiro_lista_de_veiculos=nome_ficheiro_lista_de_veiculos,
                nome_ficheiro_lista_de_utilizadores=nome_ficheiro_lista_de_utilizadores
            )
        elif op == "cn":
            id_comprador = pergunta_id(questao="Qual o id do comprador?", lista=lista_de_utilizadores)
            id_veiculo = pergunta_id(questao="Qual o id do veiculo?", lista=lista_de_veiculos)
            lista_de_compras.append([id_comprador, id_veiculo, time.time()])
        elif op == "cl":
            pass
            # todo
            # imprime_lista_de_compras()


if __name__ == "__main__":
    menu()
