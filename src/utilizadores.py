from io_terminal import imprime_lista

nome_ficheiro_lista_de_utilizadores = "lista_de_utilizadores.pk"
"""
o nome da lista é lista_de_utilizadores.pk
"""
def cria_novo_utilizador():
    """ pede os dados de um novo utilizador

    :return: dicionario com o novo utilizador, {"nome": <<nome>>, "morada": <<morada>>,"codigo postal": <<codpostal>>, "telefone": <<telefone>>, "NIF": <<NIF>>, "email": <<email>>}
    """
    # todo

    nome = input("Nome? ")
    morada = input("morada? ")
    codpostal = int(input("Codigo-Postal? "))
    telefone = int(input("telefone? "))
    nif = int(input("NIF? "))
    email = input("Email? ").upper()
    return {"nome": nome, "morada": morada, "codigo-postal": codpostal, "telefone": telefone, "nif": nif, "email": email}
"""
dados para o utilizador introduzir
"""

def imprime_lista_de_utilizadores(lista_de_utilizadores):
    """ ..."""

    imprime_lista(cabecalho="Lista de Utilizadores", lista=lista_de_utilizadores)
