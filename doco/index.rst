.. trabalho documentation master file, created by
   sphinx-quickstart on Thu Feb 11 18:20:03 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to trabalho's documentation!
====================================
.. automodule:: ../doco
   :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   MODULES

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
